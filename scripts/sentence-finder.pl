#!/usr/bin/env perl
use strict;
use Data::Dumper;

my @words=[undef, undef, undef, [], [], []];

open my $FD, '<data/corpus.txt' or die $!;

while (<$FD>){
  chomp;
  my $l = length $_;
  next if $l<4 or $l>6;
  my @a = split //, $_;
  push @{$words[$l]}, \@a; 
}
foreach (0..$#words){
   next unless $words[$_] && $_;
   my $nbWords = scalar(@{$words[$_]});
   print "$nbWords words with $_ letters\n";
}
foreach my $word3 (@{$words[6]}){
  foreach my $word2 (@{$words[5]}){
    next unless $word2->[4] eq $word3->[0] && $word2->[0] eq $word3->[5];
    foreach my $word1 (@{$words[4]}){
      next unless $word1->[0] eq $word3->[4] && $word1->[2] eq $word2->[3];
      my %hLetter=();
      $hLetter{$_}=1 foreach @{$word3}; 
      $hLetter{$_}=1 foreach @{$word2};     
      $hLetter{$_}=1 foreach @{$word1};
      next unless scalar(keys %hLetter) == 11;
      print join("", @{$word1})." ".join("", @{$word2})." ".join('',@{$word3})."\n";
    }
  }

}
