# Decyphering the Valère graffiti

![The original XVI century graffiti](graffiti.png)

Restoring a wall in Valère (Sion, Switzerland) revealed several XVI century graffiti ([more details](https://www.musees-valais.ch/musee-histoire/nouveautes/item/737-des-graffitis-du-xvie-siecle-refont-surface-a-valere.html)).
Among latin, italian, german and french tags (some about the incompetency of the local doctors...) lies one with a code.

# The problem

This looks like a classic code one symbol ~ one letter. Therefore, a set of constraints:

  * a three words sentences with words of 4, 5 and 6 characters "w1 w2 w3"
  * the last letter of w3 is the first of w2
  * the last letter of w2 is the first of w3
  * the first letter of w1 is the fifth of w3
  * the third letter of w1 is the fourth of w2
  * as we have four repeated letters and 15 characters overall, there should be 11 different characters in the sentence

Looking at the structure and time, an assumption was made that it should be latin.

# The solution

We extracted a latin corpus from the Vulgate (making another aassumption that there shall be all the possible latin words in the text) and brute force the possible combination.

2824 solutions were [found](results.txt)

A native latin reader has to go through them to check if some make any sense...

# The code

## Building a latin word corpus

    # downloading locally an html Vulgate version
    get -r np -l 1 https://www.fourmilab.ch/etexts/www/Vulgate/

    # extracting the actual text from html and calling a couple of regular expressions to build a list of unique words
    for f in www.fourmilab.ch/etexts/www/Vulgate/*.html
    do 
      html2text -nobs $f | perl -nle 'print $_ unless /(===|\*\*\*|Next:)/i'
    done | \
    perl -nle '$l = $_; print uc $1 while $l=~/([a-zA-Z]+)/g' | \
    sort -u > corpus.txt

## Extracting potential candidates
There is no check regarding an eventual meaning of the latin sentences. 
A [Perl script](scripts/sentence-finder.pl) goes through the corpus and output the three words list satisfying the constraints.

    scripts/sentence-finder.pl | sort > results.txt
